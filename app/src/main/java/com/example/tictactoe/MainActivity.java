package com.example.tictactoe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activity which presents the Tic Tac Toe game to the user
 */
public class MainActivity extends AppCompatActivity {

    private TextView mSquares[][];
    private final String CHAR_O="O";
    private final String CHAR_X="X";
    private final String BUNDLE_VALUES_ARRAY="bundleValuesArray";
    private final String BUNDLE_PLAYER_1_MOVES="bundlePlayer1Moves";
    private final String BUNDLE_PLAYER_2_MOVES="bundlePlayer2Moves";

    // last entered char, start with O
    String mLastEntered=CHAR_O;

    // array to store the entered values, stores X as 1 and O as 2
    private int[][] checkedValues;

    // maintains count of player moves, player 1 plays for O and 2 plays for X
    private int mPlayer1MoveCount,mPlayer2MoveCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        // check if we already have mSquares state set i.e. coming from an orientation change
        if(savedInstanceState!=null &&
                savedInstanceState.getSerializable(BUNDLE_VALUES_ARRAY)!=null){

            checkedValues= (int[][]) savedInstanceState.getSerializable(BUNDLE_VALUES_ARRAY);
            setRetainGameState();

            // get the player moves
            mPlayer1MoveCount=savedInstanceState.getInt(BUNDLE_PLAYER_1_MOVES);
            mPlayer2MoveCount=savedInstanceState.getInt(BUNDLE_PLAYER_2_MOVES);
        }

    }

    /**
     * Sets / retains the game values during an orientation change
     */
    private void setRetainGameState(){

        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){

                if(checkedValues[i][j]==1){
                    mSquares[i][j].setText(CHAR_X);
                }
                else if(checkedValues[i][j]==2){
                    mSquares[i][j].setText(CHAR_O);

                }
            }
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save the values array to persist data
        outState.putSerializable(BUNDLE_VALUES_ARRAY,checkedValues);
        outState.putInt(BUNDLE_PLAYER_1_MOVES,mPlayer1MoveCount);
        outState.putInt(BUNDLE_PLAYER_2_MOVES,mPlayer2MoveCount);
    }

    /**
     * Init the views
     */
    private void init(){

        mSquares = new TextView[3][3];
        checkedValues = new int[3][3];

        mSquares[0][0]=(TextView)findViewById(R.id.tv1);
        mSquares[0][1]=(TextView)findViewById(R.id.tv2);
        mSquares[0][2]=(TextView)findViewById(R.id.tv3);
        mSquares[1][0]=(TextView)findViewById(R.id.tv4);
        mSquares[1][1]=(TextView)findViewById(R.id.tv5);
        mSquares[1][2]=(TextView)findViewById(R.id.tv6);
        mSquares[2][0]=(TextView)findViewById(R.id.tv7);
        mSquares[2][1]=(TextView)findViewById(R.id.tv8);
        mSquares[2][2]=(TextView)findViewById(R.id.tv9);

        // set the click listeners
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                mSquares[i][j].setOnClickListener(new SquareClickListener(i,j));
            }
        }

    }

    /**
     * Checks / marks the square
     * @param x the x position / coordinate
     * @param y the y position / coordinate
     */
    private void checkSquare(int x,int y){

        // only set value if empty, user cannot fill in already filled square
        if(TextUtils.isEmpty(mSquares[x][y].getText())){

            if(mLastEntered.equals(CHAR_O)){
                mSquares[x][y].setText(CHAR_X);
                checkedValues[x][y]=1;
                mPlayer2MoveCount++;
                mLastEntered=CHAR_X;
            }
            else {
                mSquares[x][y].setText(CHAR_O);
                checkedValues[x][y]=2;
                mPlayer1MoveCount++;
                mLastEntered=CHAR_O;
            }
        }
    }


    /**
     * Checks if either of the players have finished / completed the game
     */
    private void checkGameState(){

        // player 2 check
        if((checkedValues[0][0]==1 && checkedValues[0][1]==1 && checkedValues[0][2]==1)
                ||(checkedValues[1][0]==1 && checkedValues[1][1]==1 && checkedValues[1][2]==1)
                ||(checkedValues[2][0]==1 && checkedValues[2][1]==1 && checkedValues[2][2]==1)
                ||(checkedValues[0][0]==1 && checkedValues[1][0]==1 && checkedValues[2][0]==1)
                ||(checkedValues[0][1]==1 && checkedValues[1][1]==1 && checkedValues[2][1]==1)
                ||(checkedValues[0][2]==1 && checkedValues[1][2]==1 && checkedValues[2][2]==1)
                ||(checkedValues[0][0]==1 && checkedValues[1][1]==1 && checkedValues[2][2]==1)
                ||(checkedValues[0][2]==1 && checkedValues[1][1]==1 && checkedValues[2][0]==1)) {

            Toast.makeText(MainActivity.this,
                    getResources().getString(R.string.player_2_wins),Toast.LENGTH_SHORT)
                    .show();

            resetGame();

        } // player 1 check
        else if((checkedValues[0][0]==2 && checkedValues[0][1]==2 && checkedValues[0][2]==2)
                ||(checkedValues[1][0]==2 && checkedValues[1][1]==2 && checkedValues[1][2]==2)
                ||(checkedValues[2][0]==2 && checkedValues[2][1]==2 && checkedValues[2][2]==2)
                ||(checkedValues[0][0]==2 && checkedValues[1][0]==2 && checkedValues[2][0]==2)
                ||(checkedValues[0][1]==2 && checkedValues[1][1]==2 && checkedValues[2][1]==2)
                ||(checkedValues[0][2]==2 && checkedValues[1][2]==2 && checkedValues[2][2]==2)
                ||(checkedValues[0][0]==2 && checkedValues[1][1]==2 && checkedValues[2][2]==2)
                ||(checkedValues[0][2]==2 && checkedValues[1][1]==2 && checkedValues[2][0]==2)){

            Toast.makeText(MainActivity.this,
                    getResources().getString(R.string.player_1_wins),Toast.LENGTH_SHORT)
                    .show();

            resetGame();
        }
        else if(mPlayer1MoveCount+mPlayer2MoveCount==9){

            // match drawn
            Toast.makeText(MainActivity.this,
                    getResources().getString(R.string.match_drawn),Toast.LENGTH_SHORT)
                    .show();

            resetGame();
        }
    }

    /**
     * Resets the game
     */
    private void resetGame(){

        mPlayer1MoveCount=0;mPlayer2MoveCount=0;

        // empty the text views and the stored checked values array
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                mSquares[i][j].setText("");
                checkedValues[i][j]=0;
            }
        }

    }


    /**
     * Handles the click event of the textviews
     */
    class SquareClickListener implements View.OnClickListener{

        int xPos,yPos;

        SquareClickListener(int x,int y){
            xPos=x;
            yPos=y;
        }

        @Override
        public void onClick(View v) {
            checkSquare(xPos,yPos);

            // check the game state after each move
            checkGameState();
        }
    }
}
